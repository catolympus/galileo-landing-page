[![Commitizen friendly](https://img.shields.io/badge/commitizen-friendly-brightgreen.svg)](http://commitizen.github.io/cz-cli/)

# GALILEO LANDING PAGE

Stack used:

- React JS
- Next JS
- Styled Components
- I18n
- Husky
- Prettier Standard

Code style:

- Standard

## USAGE

Clone the project and install npm dependencies.

`yarn`

Run development

`yarn dev`

Application will be started on localhost:3000

## Authors

- [geceramirez](https://www.linkedin.com/in/gian-ramirez/)
- [rafaelbecks93](https://www.linkedin.com/in/rafaelbecks/)
