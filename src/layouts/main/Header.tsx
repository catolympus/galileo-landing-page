import React from 'react'
import Link from 'next/link'

import {
  NavBar,
  LeftSection,
  NavbarItem,
  RightSection,
  Logo,
  LogoWrapper
} from './styles'

const Header = () => (
  <NavBar>
    <LeftSection>
      <LogoWrapper>
        <Logo />
      </LogoWrapper>
    </LeftSection>
    <RightSection>
      <Link href='/start'>
        <NavbarItem>Start now</NavbarItem>
      </Link>
      <Link href='/advantages'>
        <NavbarItem>Advantages</NavbarItem>
      </Link>
      <Link href='/pricing'>
        <NavbarItem>Pricing</NavbarItem>
      </Link>
      <Link href='/about-us'>
        <NavbarItem>About us</NavbarItem>
      </Link>
    </RightSection>
  </NavBar>
)

export default Header
