import styled from 'styled-components'
import breakpoint from 'styled-components-breakpoint'

import logoImg from '@assets/logo.svg'

const NavBar = styled.div`
  background: ${({ theme }) => theme.colors.white};
  font-size: 13px;
  display: flex;
  justify-content: center;

  ${breakpoint('lg')`
    justify-content: space-between;
  `}
`

const NavbarItem = styled.div`
  margin-left: 30px;
  cursor: pointer;
`

const LogoWrapper = styled.div`
  padding: 10px 0px;
  cursor: pointer;
`

const LeftSection = styled.div`
  display: flex;
  justify-content: center;

  ${breakpoint('lg')`
    justify-content: space-between;
    width: 50%;
    padding-left: 80px;
`}
`

const RightSection = styled.div`
  display: none;

  ${breakpoint('lg')`
    display: flex;
    justify-content: flex-end;
    align-items: center;
    width: 50%;
    padding-right: 80px;
    color: ${({ theme }) => theme.colors.main};
  `}
`

const Logo = styled.div`
  background: url(${logoImg});
  background-size: contain;
  background-repeat: no-repeat;
  background-position: center;
  width: 120px;
  height: 50px;
`

export { NavBar, LeftSection, RightSection, NavbarItem, Logo, LogoWrapper }
