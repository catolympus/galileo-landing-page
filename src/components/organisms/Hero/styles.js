import styled from 'styled-components'
import breakpoint from 'styled-components-breakpoint'

import heroImg from '@assets/hero.svg'

const Container = styled.div`
  color: ${({ theme }) => theme.colors.main};
  display: grid;
  grid-template-areas:
    'text'
    'right'
    'button';
  grid-template-rows: 1fr 1fr 50px;

  ${breakpoint('md')`
        margin-top: 40px;
        grid-template-areas: 
                        ". . right ."
                        ". text right ."
                       ". button  right ."
                       ". .  right .";
        grid-template-rows: 50px 1fr 50px 50px;
        grid-template-columns: 80px 1fr 2fr 40px;
    `}

  ${breakpoint('xl')`
        grid-template-columns: 180px 1fr 2fr 90px;
    `}
`

const LeftSection = styled.div`
  grid-area: text;
  display: flex;
  justify-content: center;
  text-align: center;

  ${breakpoint('md')`
    text-align: left;
`}
`
const TextWrapper = styled.div`
  width: 245px;
  line-height: 1.2;
  ${breakpoint('md')`
        margin-top: 0px;
        width: 100%;
    `}
`

const RightSection = styled.div`
  grid-area: right;
  display: flex;
  justify-content: center;
`

const HeroImg = styled.div`
  background: url(${heroImg});
  background-repeat: no-repeat;
  background-size: contain;
  background-position: center;
  height: 210px;
  width: 210px;
  ${breakpoint('md')`
        height: 391px;
        width: 320px;
    `}
  ${breakpoint('lg')`
        height: 391px;
        width: 520px;
    `}
`

const ButtonWrapper = styled.div`
  grid-area: button;
  width: 300px;
  height: 50px;
  font-size: 21;
  ${breakpoint('md')`
        text-align: left;
        width: 140px;
        height: 40px;
    `}
`

const CallToActionSection = styled.div`
  grid-area: button;
  display: flex;
  justify-content: center;
  ${breakpoint('md')`
        justify-content: flex-start;
    `}
`

export {
  Container,
  LeftSection,
  RightSection,
  HeroImg,
  ButtonWrapper,
  TextWrapper,
  CallToActionSection
}
