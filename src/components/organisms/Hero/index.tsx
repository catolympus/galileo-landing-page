import React from 'react'

import { Typography, Button } from '@components'
import {
  Container,
  LeftSection,
  RightSection,
  HeroImg,
  ButtonWrapper,
  TextWrapper,
  CallToActionSection
} from './styles'

const Hero = () => (
  <Container>
    <LeftSection>
      <TextWrapper>
        <Typography.Display>
          Launch Startup ideas really fast
        </Typography.Display>
        <Typography.H3 regular>
          Get your MVP Product in less than 3 weeks
        </Typography.H3>
      </TextWrapper>
    </LeftSection>
    <CallToActionSection>
      <ButtonWrapper>
        <Button>Start now</Button>
      </ButtonWrapper>
    </CallToActionSection>
    <RightSection>
      <HeroImg />
    </RightSection>
  </Container>
)

export default Hero
