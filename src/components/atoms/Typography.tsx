import React from 'react'
import styled from 'styled-components'

interface HProps {
  readonly regular?: boolean;
};

const H1 = styled.h1<HProps>`
  font-size: 2em;
  color: ${({ theme }) => theme.colors.main};
  font-weight: ${({ regular }) => regular ? '500' : '900'};
`

const H2 = styled(props => <H1 {...props} />)`
  font-size: 1.5em;
`

const H3 = styled(props => <H1 {...props} />)`
  font-size: 1.17em;
`

const H4 = styled(props => <H1 {...props} />)`
  font-size: 1em;
`

const H5 = styled(props => <H1 {...props} />)`
  font-size: 0.83em;
`

const H6 = styled(props => <H1 {...props} />)`
  font-size: 0.67em;
`

const Display = styled(props => <H1 {...props} />)`
  font-size: 40px;
`

const Paragraph = styled.p`
  font-size: 13px;
`

export default { H1, H2, H3, H4, H5, H6, Display, Paragraph }
