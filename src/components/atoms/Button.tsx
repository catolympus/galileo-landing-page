import styled from 'styled-components'
import breakpoint from 'styled-components-breakpoint'

const Button = styled.button`
  background: ${({ theme }) => theme.colors.featured};
  border: none;
  color: ${({ theme }) => theme.colors.white};
  border-radius: 25px;
  min-width: 70px;
  padding: 5px 10px;
  cursor: pointer;
  width: 100%;
  height: 100%;
  font-size: 21px;
  ${breakpoint('md')`
    font-size: 14px;
  `}
`

export default Button
