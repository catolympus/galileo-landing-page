import { DefaultTheme } from 'styled-components'

const MainTheme: DefaultTheme = {
  colors: {
    main: '#5046AF',
    secondary: '#1E5A90',
    weak: '#B0B0FF',
    featured: '#FE20C2',
    white: '#fff',
    black: '#000'
  },
  breakpoints: {
    xs: 0,
    sm: 576,
    md: 768,
    lg: 992,
    xl: 1200
  }
}

export default MainTheme