import 'styled-components'

declare module 'styled-components' {
  export interface DefaultTheme {
    colors: {
      main: string,
      secondary: string,
      weak: string,
      featured: string,
      white: string,
      black: string
    }

    breakpoints: {
      xs: number,
      sm: number,
      md: number,
      lg: number,
      xl: number
    }
  }
}