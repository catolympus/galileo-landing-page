import App from 'next/app'
import React from 'react'
import { ThemeProvider } from 'styled-components'
import { appWithTranslation } from '../i18n'
import { MainTheme } from "../src/themes";

class MyApp extends App {
  render () {
    const { Component, pageProps } = this.props
    return (
      <ThemeProvider theme={MainTheme}>
        <Component {...pageProps} />
      </ThemeProvider>
    )
  }
}

export default appWithTranslation(MyApp)
